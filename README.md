# TE-SAS-dashboard

##What is it for?
This is a dashboard for visualization of TE's Smart Access System. Every time someone request access to one of TE's rooms, a notification will be shown.

##How does it work?
dashboard.html is the main page. It triggers demo.js which will create a MQTT client subscribed to TE/SAS/access topic. Every time a request is made, the MQTT client will lisent to the answer.

##Contact
If you have any comment or doubt, please contact soleyda.manrique@titoma.com 

